.. AnnoTree documentation master file, created by
   sphinx-quickstart on Tue Mar 19 09:06:14 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

========
AnnoTree
========

.. caution:: This documentation server is in an early stage of development.


.. toctree::
   :maxdepth: 1
   :caption: Using AnnoTree:
   :hidden:

   usage_info/features
   usage_info/examples


.. toctree::
   :maxdepth: 1
   :caption: AnnoTree Setup:
   :hidden:

   setup/custom_data
   setup/annotree_data


.. toctree::
   :maxdepth: 1
   :caption: Project repo README's:
   :hidden:

   annotree-scripts/readme.md
   annotree-backend/readme.md
   annotree-frontend/README.md
   annotree-landing-page/README.md


.. toctree::
   :maxdepth: 1
   :caption: Project Info:
   :hidden:

   project_info/citation
   project_info/contributing
   project_info/license
