.. setup-custom_data:

=========================
AnnoTree with Custom Data
=========================

The use of custom taxonomic, phylogenetic, proteomic, or annotation data only affects the database preparation step.
All other application components operate the same way as the production version of AnnoTree.

^^^^^^^^^^^^^^^^
Prepare Database
^^^^^^^^^^^^^^^^
Documentation for database preparation is located within the :ref:`AnnoTree Database Scripts` section.
Keep track of the name of the new database for the backend configuration.

^^^^^^^^^^^^^^^^^
Configure Backend
^^^^^^^^^^^^^^^^^
Documentation for backend configuration is located within the :ref:`AnnoTree Backend` section.
Keep track of the URL that the backend is being served to for the frontend configuration.

^^^^^^^^^^^^^^^^^^
Configure Frontend
^^^^^^^^^^^^^^^^^^
Documentation for the frontend configuration is located within the :ref:`AnnoTree Frontend` section.
